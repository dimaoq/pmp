function ListNode(id, geotag, placemark, isChecked){
		this.getID = function(){
			return id;
		}
		
		this.getGeotag = function(){
			return geotag;
		}
		
		this.getPlacemark = function(){
			return placemark;
		}
		
		this.setPlacemark = function(newPlacemark){
			placemark = newPlacemark;
		}
		
		this.isChecked = function(){
			return isChecked;
		}
		
		this.toggleCheck = function(){
			isChecked = !isChecked;
		}
}
	
function GeotagList(){
	var list = [];
	var curID = 0;
	
	this.size = function(){
		return list.length;
	}
	
	this.getElem = function(index){
		return list[index];
	}
	
	this.getElemByID = function(id){
		for (var i = 0; i < list.length; i++){
			if (list[i].getID() == id){
				return list[i];
			}
		}
		
		return null;
	}
	
	this.deleteElemByID = function(id){
		for (var i = 0; i < list.length; i++){
			if (list[i].getID() == id){
				list.splice(i, 1);
				break;
			}
		}
	}
	
	this.deleteAllElements = function(){
		list.length = 0;
	}
	
	this.getID = function(){
		return curID;
	}
	
	this.addGeotag = function(curGeotag, placemark, isChecked){
		var newNode = new ListNode(++curID, curGeotag, placemark, isChecked);
		list.push(newNode);
	}
}