var GeotagStorage = new function(){
	
	this.size = function(){
		var curSize = localStorage.getItem('jsonsize');
		if (undefined != curSize){
			return curSize;
		}
		
		return 0;
	}
	
	this.getObjects = function(){
		if (this.size() > 0){
			return JSON.parse(localStorage.getItem('objects'));
		}
		
		return null;
	}
	
	this.saveAllObjects = function(objectsToSave){
		var curSize = objectsToSave.size();
		localStorage.setItem('jsonsize', curSize);
		if (curSize > 0){
			var arrayToSave = [];
			for (var i = 0; i < curSize; i++){
				var elem = {lat: objectsToSave.getElem(i).getGeotag().getLatitude(), lng: objectsToSave.getElem(i).getGeotag().getLongitude(),
							name: objectsToSave.getElem(i).getGeotag().getName(), checked: objectsToSave.getElem(i).isChecked()};
				arrayToSave.push(elem);
			}
			localStorage.setItem('objects', JSON.stringify(arrayToSave));
		}
	}
	
	this.dropAllData = function(){
		localStorage.clear();
	}
}