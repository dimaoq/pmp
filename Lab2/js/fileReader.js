function GeotagReader(){
	
	this.init = function(callbackOnRead){
		if (Modernizr.draganddrop){
			$("#geotags").on('dragover', function(e){
				e.preventDefault();
				e.stopPropagation();
			});
			$("#geotags").on('dragenter', function(e){
				e.preventDefault();
				e.stopPropagation();
			});
			$("#geotags").on('drop', callbackOnRead);
		}
		else{
			alert("Drag&drop is not supported");
		}
	}
	
}