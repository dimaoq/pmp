var AppController = new function(){
	var map = null;
	var list = null;
	var fileHandler = null;
	var curLocation = null;
	
	function createPlacemark(curGeotag){
		return new ymaps.Placemark([curGeotag.getLatitude(), curGeotag.getLongitude()],{
			hintContent: curGeotag.getName()
		});
	}
	
	function updateView(){
		if (null != list){
			for (var i = 0; i < list.size(); i++){
				var elem = list.getElem(i);
				var elemID = elem.getID();
				if (null != elem.getPlacemark()){
					if (!elem.isChecked()){
						map.geoObjects.remove(elem.getPlacemark());
						elem.setPlacemark(null);
						
						$('#chkBox' + elemID).prop('checked', false);
					}
				}
				else{
					if (elem.isChecked()){
						var newPlacemark = createPlacemark(elem.getGeotag());
						elem.setPlacemark(newPlacemark);
						map.geoObjects.add(newPlacemark);
						
						$('#chkBox' + elemID).prop('checked', true);
					}
				}
			}
			
			GeotagStorage.saveAllObjects(list);
		}
	}
	
	function addNewGeotag(curGeotag, isVisible){
		if (null != list){
			list.addGeotag(curGeotag, null, isVisible);
			
			var header = $("<span>", {contenteditable: "true"}).html(curGeotag.getName());
			var chkBox = "<input type=\"checkbox\" id=\"";
			chkBox += "chkBox" + list.getID();
			chkBox += "\" onClick=\"AppController.changeCheck(this)\"";
			if (isVisible){
				chkBox += "checked> <br>";
			}
			else{
				chkBox += "> <br>";
			}
			var btnDelete = "<input type=\"button\" onClick=\"AppController.deleteTag(this)\" value=\"Delete geotag\">";
			
			var form = $("<form></form>");
			form.append(header, chkBox, btnDelete);
			
			var className = "gt" + list.getID();
			$("<div>", {class: className}).append(form).appendTo("#geotags");
					
			updateView();
		}
	}
	
	function setCurLocation(position){
		if (null != curLocation){
			map.geoObjects.remove(curLocation);
		}
		
		curLocation= new ymaps.Placemark([position.coords.latitude, position.coords.longitude],{
				hintContent: 'Current location'
		});
			
		map.geoObjects.add(curLocation);
		map.setCenter([position.coords.latitude, position.coords.longitude], 7);
	}
	
	function readFromFile(e){
		event.preventDefault();
		var file = e.originalEvent.dataTransfer.files[0];
		var reader = new FileReader();
		reader.onload = function(ev){
			var res = ev.target.result;
			var objs = JSON.parse(res);
			for (var i = 0; i < list.size(); i++){
				var elem = list.getElem(i);
				if (null != elem.getPlacemark()){
					map.geoObjects.remove(elem.getPlacemark());
					$(".gt" + elem.getID()).remove();
				}
			}
			list.deleteAllElements();
			
			for (var i = 0; i < objs.length; i++){
				var obj = objs[i];
				var name = obj["name"];
				var coords = obj["coordinates"];
				var lat = coords["latitude"];
				var lng = coords["longitude"];
				
				var geotagToAdd = new Geotag(lat, lng, name);
				addNewGeotag(geotagToAdd, true);
			}
		};
		reader.readAsText(file, "UTF-8");
	}
	
	this.initApplication = function(){
		list = new GeotagList();
		map = new ymaps.Map("map", {
			center: [55.76, 37.64],
            zoom: 7
		});
		fileHandler = new GeotagReader();
		fileHandler.init(readFromFile);
		
		if (GeotagStorage.size() > 0){
			var objs = GeotagStorage.getObjects();
			for (var i = 0; i < objs.length; i++){
				var obj = objs[i];
				var name = obj["name"];
				var lat = obj["lat"];
				var lng = obj["lng"];
				var checked = obj["checked"];
				
				var geotagToAdd = new Geotag(lat, lng, name);
				addNewGeotag(geotagToAdd, checked);
			}
			
			updateView();
		}
	}
	
	this.addNewGeotagFromForm = function(){
		event.preventDefault();
		
		var lat = $('#txtLat').val();
		var lng = $('#txtLng').val();
		var name = $('#txtName').val();
		
		var geotagToAdd = new Geotag(lat, lng, name);
		addNewGeotag(geotagToAdd, true);
		$('#txtName').val("");
		
		return true;
	}
	
	function deg2rad(deg) {
	  return deg * (Math.PI/180)
	}
	
	function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
	  var R = 6371;
	  var dLat = deg2rad(lat2-lat1);
	  var dLon = deg2rad(lon2-lon1); 
	  var a = 
		Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
		Math.sin(dLon/2) * Math.sin(dLon/2)
		; 
	  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	  var d = R * c;
	  return d;
	}

	function selectCorrectGeotags(locationStart){
		var maxDist = parseInt($('#txtRadius').val());
		
		var latFrom = locationStart.coords.latitude;
		var longFrom = locationStart.coords.longitude;
		
		for (var i = 0; i < list.size(); i++){
			var elem = list.getElem(i);
			if (maxDist >= getDistanceFromLatLonInKm(latFrom, longFrom, elem.getGeotag().getLatitude(), elem.getGeotag().getLongitude())){
				if (!elem.isChecked()){
					elem.toggleCheck();
				}
			}
			else{
				if (elem.isChecked()){
					elem.toggleCheck();
				}
			}
		}
		
		updateView();
	}
	
	this.showInRadius = function(){
		event.preventDefault();
		navigationWrapper.getCurrentLocation(selectCorrectGeotags);
		
		return true;
	}
	
	this.showAll = function(){
		for (var i = 0; i < list.size(); i++){
			var elem = list.getElem(i);
			if (!elem.isChecked()){
				elem.toggleCheck();
			}
		}
		
		updateView();
	}
	
	this.showCurrentLocation = function(){
		navigationWrapper.getCurrentLocation(setCurLocation);
	}
	
	this.deleteTag = function(thisElem){
		var nClass = $(thisElem).parent().parent('div').attr('class');
		var id = parseInt(nClass.substring(2));
		var elem = list.getElemByID(id);
		if (null != elem){
			if (null != elem.getPlacemark()){
				map.geoObjects.remove(elem.getPlacemark());
				elem.setPlacemark(null);
			}
			list.deleteElemByID(id);
			$(thisElem).parent().parent('div').remove();
			updateView();
		}
	}
	
	this.changeCheck = function(thisElem){
		var nClass = $(thisElem).parent().parent('div').attr('class');
		var id = parseInt(nClass.substring(2));
		var elem = list.getElemByID(id);
		if (null != elem){
			elem.toggleCheck();
			updateView();
		}
	}
}

ymaps.ready(AppController.initApplication);