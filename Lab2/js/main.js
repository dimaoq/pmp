var navigationWrapper = new function(){
	this.getCurrentLocation = function(successCallback){
		if (Modernizr.geolocation){
			navigator.geolocation.getCurrentPosition(successCallback, 
			function(e) {
				switch (e.code) {
					case e.PERMISSION_DENIED:
						alert("Permission denied");
						break;
					case e.POSITION_UNAVAILABLE:
						alert("Position unavaliable");
						break;
					case e.TIMEOUT:
						alert("The application has timed out");
						break;
					default:
						alert("There was a horrible Geolocation");
				}
			},
			{ 
				enableHighAccuracy: true,
				timeout: 60000,
				maximumAge: 0
			});
		}
		else{
			alert("No support of geolocation");
		}
	}
}