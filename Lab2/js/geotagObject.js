function Geotag(latitude, longitude, name){
	
	this.getLatitude = function(){
		return latitude;
	}
	
	this.getLongitude = function(){
		return longitude;
	}
	
	this.getName = function(){
		return name;
	}
}