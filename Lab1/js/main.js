var grabber = new function(){
	var mainUrl = "http://localhost:8888/1.1/";
	var curResults = "";
	var curPage = 0;
	var maxPageCount = -1;
	
	this.getTweets = function(){
		event.preventDefault();
		
		var userName = $('#username').val();
		var userID = $('#userid').val();
		if (userName.length == 0 && userID.length == 0){
			alert("At least one field must be filled");
			return false;
		}
		
		var tweetCount = $('#tweetCount').val();
		var queryData = {
			count: tweetCount,
			include_rts: 1,
			include_entities: 1
		};
		if (userName.length > 0){
			queryData["screen_name"] = userName;
		}
		else{
			queryData["user_id"] = userID;
		}
		
		var queryUrl = mainUrl + "statuses/user_timeline.json";
		$.ajax({
			url: queryUrl,
			type: "GET",
			dataType: "jsonp",
			data: queryData,
			success: function(result){
				$("#tweets").empty();
				$("#filterBtn").attr("disabled", false);
				$("#prevPage").attr("disabled", false);
				$("#nextPage").attr("disabled", false);
				
				curResults = result;
				curPage = 0;
				grabber.filter(1);
			},
			error: function(){
				console.log("ERROR WHILE GETTING TWEETS");
			}
		});
		
		return true;
	}
	
	this.filter = function(fromStartPage){
		event.preventDefault();
		
		if (fromStartPage == 1){
			curPage = 0;
		}
		
		if (curResults.length == 0){
			console.log("curResults == 0");
			return false;
		}
		
		var dateFromStr = $('#dateFrom').val();
		var dateToStr = $('#dateTo').val();
		var dateFrom = new Date(Date.parse(dateFromStr));
		var dateTo = new Date(Date.parse(dateToStr));
		if (null == dateFrom){
			console.log("dateFrom = null " + dateFrom); 
		}
		
		if (null == dateTo){
			console.log("dateTo = null " + dateTo); 
		}
		else{
			dateTo.setDate(dateTo.getDate() + 1);
			dateTo.setSeconds(dateTo.getSeconds() - 1);
		}
		
		var perPageStr = $('#tweetsPerPage').val();
		var tweetsPerPage = parseInt(perPageStr, 10);
		if (isNaN(tweetsPerPage)){
			console.log("Can't parse count of tweets per page");
			return false;
		}
		var curPageCount = 0;
		var curTweetCount = 0;
		
		$("#tweets").empty();
		$.each(curResults, function(i, obj){
			var tweetDateStr = obj.created_at;
			var tweetDate = new Date(Date.parse(tweetDateStr));
			if (null == tweetDate){
				console.log("can't parse tweet date = " + tweetDateStr);
			}
			else{
				var valid = true;
				if (null != dateFrom){
					if (dateFrom > tweetDate){
						valid = false;
					}
				}
				
				if (null != dateTo){
					if (dateTo < tweetDate){
						valid = false;
					}
				}
				
				if (valid){
					if (curTweetCount > 0 && ((curTweetCount % tweetsPerPage) == 0)){
						curPageCount++;
					}
					curTweetCount++;
					
					if (curPageCount != curPage){
						return;
					}
			
					var header = $("<h1>").html(obj.user.name);
					var message = $("<p>").html(obj.text);
					var details = $("<details>").append("<span>").html(obj.created_at);
					
					$("<article>", {id: obj.id}).append(header, message, details).appendTo("#tweets");
				}
			}
		});
		maxPageCount = curPageCount;
		
		return true;		
	}
	
	this.goNextPage = function(){
		if (curPage < maxPageCount){
			curPage++;
			this.filter(0);
		}
	}
	
	this.goPrevPage = function(){
		if (curPage > 0){
			curPage--;
			this.filter(0);
		}
	}
}