import React from 'react';
import ReactDOM from 'react-dom';
import ControlsComponent from './ControlsComponent';
import './index.css';

ReactDOM.render(
  <ControlsComponent />,
  document.getElementById('root')
);
