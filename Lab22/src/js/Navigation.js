function Navigation () {
	this.getCurrentLocation = function (successCallback) {
		navigator.geolocation.getCurrentPosition(successCallback, function (e) {
			switch (e.code) {
				case e.PERMISSION_DENIED:
					alert("Permission denied");
					break;
				case e.POSITION_UNAVAILABLE:
					alert("Position unavaliable");
					break;
				case e.TIMEOUT:
					alert("The application has timed out");
					break;
				default:
					alert("There was a horrible Geolocation");
			}
		}, {
			enableHighAccuracy: true,
			timeout: 60000,
			maximumAge: 0
		});
	}
	
	function deg2rad(deg) {
	  return deg * (Math.PI/180)
	}
	
	function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
	  var R = 6371;
	  var dLat = deg2rad(lat2-lat1);
	  var dLon = deg2rad(lon2-lon1); 
	  var a = 
		Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
		Math.sin(dLon/2) * Math.sin(dLon/2)
		; 
	  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	  var d = R * c;
	  return d;
	}
	
	this.isInRadiusToCurrentPosition = function(lat1, lon1, lat2, lon2, radius){
		return (getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) <= radius);
	}
};

export default Navigation;