function ListNode(id, geotag, _isChecked) {
	this.getID = function () {
		return id;
	};

	this.getGeotag = function () {
		return geotag;
	};

	this.isChecked = function () {
		return _isChecked;
	};

	this.toggleCheck = function () {
		_isChecked = !_isChecked;
	};
}

function GeotagList() {
	var list = [];
	var curID = 0;

	this.getSize = function () {
		return list.length;
	};

	this.getElem = function (index) {
		return list[index];
	};

	this.deleteElemByID = function (id) {
		for (var i = 0; i < list.length; i++) {
			if (list[i].getID() === id) {
				list.splice(i, 1);
				break;
			}
		}
	};

	this.getList = function(){
		return list;
	}
	
	this.deleteAllElements = function () {
		list.length = 0;
	};

	this.addGeotag = function (curGeotag, isChecked) {
		var newNode = new ListNode(++curID, curGeotag, isChecked);
		list.push(newNode);
	};
}

export default GeotagList;