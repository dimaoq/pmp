function Geotag(latitude, longitude, name) {

	this.getLatitude = function () {
		return Number(latitude);
	};

	this.getLongitude = function () {
		return Number(longitude);
	};

	this.getName = function () {
		return name;
	};
}

export default Geotag;