import Geotag from './js/GeotagObject';
import GeotagList from './js/GeotagList';
import Navigation from './js/Navigation';
import GeotagStorage from './js/GeotagStorage';

function AppController() {
	var geotagList = null;
	var nav = null;
	var storage = null;
	var curPos = null;
	
	this.init = function(){
		geotagList = new GeotagList();
		nav = new Navigation();
		storage = new GeotagStorage();
		
		if (storage.isPresent() && storage.getSize() > 0){
			var objs = storage.getObjects();
			
			for (var i = 0; i < objs.length; i++){
				var obj = objs[i];
				var name = obj["name"];
				var lat = obj["lat"];
				var lng = obj["lng"];
				var checked = obj["checked"];
				
				var geotagToAdd = new Geotag(lat, lng, name);
				geotagList.addGeotag(geotagToAdd, checked);
			}
		}
	}
	
	this.addNewGeotag = function(lat, lon, name){
		var geotag = new Geotag(lat, lon, name);
		geotagList.addGeotag(geotag, true);
		
		if (storage.isPresent()){
			storage.saveAllObjects(geotagList);
		}
	}
	
	this.removeGeotag = function(id){
		geotagList.deleteElemByID(id);
		
		if (storage.isPresent()){
			storage.saveAllObjects(geotagList);
		}
	}
	
	this.readFile = function(e){
		geotagList.deleteAllElements();
			
		var res = e.target.result;
		var objs = JSON.parse(res);
		for (var i = 0; i < objs.length; i++){
			var obj = objs[i];
			var name = obj["name"];
			var coords = obj["coordinates"];
			var lat = coords["latitude"];
			var lng = coords["longitude"];
				
			var geotagToAdd = new Geotag(lat, lng, name);
			geotagList.addGeotag(geotagToAdd, true);
		}
	}
	
	this.showAllGeotags = function(){
		var exactList = geotagList.getList();
		for (var node of exactList){
			if (!node.isChecked()){
				node.toggleCheck();
			}
		}
		
		if (storage.isPresent()){
			storage.saveAllObjects(geotagList);
		}
	}
	
	this.showGeotagsInRadius = function(position, radius){
		var exactList = geotagList.getList();
		for (var node of exactList){
			if (nav.isInRadiusToCurrentPosition(node.getGeotag().getLatitude(), node.getGeotag().getLongitude(), 
												position.coords.latitude, position.coords.longitude, radius)){
				if (!node.isChecked()){
					node.toggleCheck();
				}
			}
			else{
				if (node.isChecked()){
					node.toggleCheck();
				}
			}
		}
		
		if (storage.isPresent()){
			storage.saveAllObjects(geotagList);
		}
	}
	
	this.checkGeotag = function(index){
		geotagList.getElem(index).toggleCheck();
		
		if (storage.isPresent()){
			storage.saveAllObjects(geotagList);
		}
	}
	
	this.setCurrentLocation = function(position){
		curPos = new Geotag(position.coords.latitude, position.coords.longitude, 'Current location');
	}

	this.getCurrentLocationCallback = function(callback){
		nav.getCurrentLocation(callback);
	}
	
	this.getGeotagList = function(){
		return geotagList;
	}
	
	this.getCurrentPosition = function(){
		return curPos;
	}
}

export default AppController;