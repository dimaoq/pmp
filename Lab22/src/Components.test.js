import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import ControlsComponent from './ControlsComponent';
import install from 'jasmine-es6';
install();

describe('ControlsComponent', function(){	
	const exampleTags = [{lat: 57, lon: 35, name: 'First'}, {lat: 14, lon: -35.2, name: 'Second'}];
	
	var component;
	var latInput, lonInput, nameInput, frmAdd;
	var btnAdd;
  
	it('correct initial render', function(){
		var elem = React.createElement(ControlsComponent, {});
		
		expect(function() {
			component = ReactTestUtils.renderIntoDocument(elem);
		}).not.toThrow();
		
		let tagList = ReactTestUtils.scryRenderedDOMComponentsWithClass(component, 'gt');
		expect(tagList.length).toEqual(0);
		
		let markerList = ReactTestUtils.scryRenderedDOMComponentsWithClass(component, 'marker');
		expect(markerList.length).toEqual(0);
		
		latInput = component.refs.inputLat;
		lonInput = component.refs.inputLon;
		nameInput = component.refs.inputName;
		btnAdd = component.refs.addGeotagBtn;
		frmAdd = component.refs.formAdd;
	});
	
	it('correct add geotag', function(){
		for (var i = 0; i < exampleTags.length; ++i){
			latInput.value = exampleTags[i].lat;
			ReactTestUtils.Simulate.change(latInput);
			
			lonInput.value = exampleTags[i].lon;
			ReactTestUtils.Simulate.change(latInput);
					
			nameInput.value = exampleTags[i].name;
			ReactTestUtils.Simulate.change(nameInput);
			
			ReactTestUtils.Simulate.submit(frmAdd);
			
			let tagList = ReactTestUtils.scryRenderedDOMComponentsWithClass(component, 'gt');
			expect(tagList.length).toEqual(i + 1);
		}
	});
	
	it('correct delete geotag', function(){
		let delButtons = ReactTestUtils.scryRenderedDOMComponentsWithClass(component, 'delButton');
		for (var i = exampleTags.length - 1; i >= 0; --i){
			ReactTestUtils.Simulate.click(delButtons[i]);
			
			let tagList = ReactTestUtils.scryRenderedDOMComponentsWithClass(component, 'gt');
			expect(tagList.length).toEqual(i);
		}
	});
});