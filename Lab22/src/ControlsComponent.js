import React, { Component } from 'react';
import './Controls.css';
import { Map, Marker } from 'yandex-map-react';
import FileDragAndDrop from 'react-file-drag-and-drop';
import AppController from './AppController';

class ControlsComponent extends Component {
	constructor(props){
		super(props);
		
		this.handleChangeLat = this.handleChangeLat.bind(this);
		this.handleChangeLon = this.handleChangeLon.bind(this);
		this.handleChangeName = this.handleChangeName.bind(this);
		this.handleChangeRad = this.handleChangeRad.bind(this);
		
		this.removeNode = this.removeNode.bind(this);
		this.checkGeotag = this.checkGeotag.bind(this);
		this.onFileRead = this.onFileRead.bind(this);
		this.onDrop = this.onDrop.bind(this);
		this.addNewGeotagFromForm = this.addNewGeotagFromForm.bind(this);
		this.showInRadius = this.showInRadius.bind(this);
		this.showAll = this.showAll.bind(this);
		this.showCurrentLocation = this.showCurrentLocation.bind(this);
		
		var ctrl = new AppController();
		ctrl.init();
		this.state={lat: 0, lon: 0, name: '', radius: 10, appCtrl: ctrl};
	}
	
	handleChangeLat(e){
		this.setState({lat: e.target.value});
	}
	
	handleChangeLon(e){
		this.setState({lon: e.target.value});
	}
	
	handleChangeName(e){
		this.setState({name: e.target.value});
	}
	
	handleChangeRad(e){
		this.setState({radius: e.target.value});
	}
	
	onFileRead(ev){
		var ctrl = this.state.appCtrl;
		ctrl.readFile(ev);
		this.setState({appCtrl: ctrl});
	}
	
	onDrop(dataTransfer){
		var file = dataTransfer.files[0];
		
		var reader = new FileReader();
		reader.onload = this.onFileRead;
		reader.readAsText(file, "UTF-8");
	}
	
	addNewGeotagFromForm(e){
		e.preventDefault();
		
		var ctrl = this.state.appCtrl;
		ctrl.addNewGeotag(this.state.lat, this.state.lon, this.state.name);
		this.setState({appCtrl: ctrl});
		
		this.refs.inputName.value = "";
	}
	
	removeNode(id){
		var ctrl = this.state.appCtrl;
		ctrl.removeGeotag(id);
		this.setState({appCtrl: ctrl});
	}
	
	checkGeotag(index){
		var ctrl = this.state.appCtrl;
		ctrl.checkGeotag(index);
		this.setState({appCtrl: ctrl});
	}
	
	showInRadius(position){		
		var ctrl = this.state.appCtrl;
		ctrl.showGeotagsInRadius(position, this.state.radius);
		this.setState({appCtrl: ctrl});
	}
	
	showAll(e){	
		var ctrl = this.state.appCtrl;
		ctrl.showAllGeotags();
		this.setState({appCtrl: ctrl});
	}
	
	showCurrentLocation(position){		
		var ctrl = this.state.appCtrl;
		ctrl.setCurrentLocation(position);
		this.setState({appCtrl: ctrl});
	}
	
	render() {
		const mapState = {
			controls: ['default']
		};
			
		return (
			<div>
				<div id="controlsContainer">
					<div id="newGeotagContainer">
						<form ref="formAdd" onSubmit={this.addNewGeotagFromForm}>
							Latitude:
							<input type="number" ref="inputLat" id="txtLat" required min={-90} max={90} defaultValue={0} step="0.001" onChange={this.handleChangeLat}/>
							Longitude: 
							<input type="number" ref="inputLon" id="txtLng" required min={-180} max={180} defaultValue={0} step="0.001" onChange={this.handleChangeLon}/>
							Name:
							<input type="text" ref="inputName" id="txtName" required maxLength={30} onChange={this.handleChangeName}/>
							<input type="submit" ref="addGeotagBtn" id="btnAddGeotag" defaultValue="Add geotag" />
						</form>
					</div>
					<div id="showContainer">
						<form onSubmit={(e) => {e.preventDefault(); this.state.appCtrl.getCurrentLocationCallback(this.showInRadius);}}>
							Radius (km): 
							<input type="number" ref="inputRad" id="txtRadius" required min={1} max={15000} defaultValue={10} onChange={this.handleChangeRad}/>
							<input type="submit" id="btnShowInRadius" defaultValue="Show in radius" />
							<input type="button" id="btnShowAll" onClick={this.showAll} defaultValue="Show all geotags" />
							<input type="button" id="btnShowCurrentLocation" onClick={(e) => {this.state.appCtrl.getCurrentLocationCallback(this.showCurrentLocation);}} defaultValue="Show current location" />
						</form>
					</div>
				</div>
			  
				<div id="mainComponent">
						<section id="geotags" >
							<FileDragAndDrop onDrop={this.onDrop}>
								{this.state.appCtrl.getGeotagList().getList().map((node, i) => (
									<div key={'gt' + i} className={'gt'}>
										<form>
											<span contentEditable="true">{node.getGeotag().getName()}</span>
											<input type="checkbox" id={'chkBox' + i} checked={node.isChecked()} onChange={(e) => {this.checkGeotag(i);}}/> <br/>
											<input type="button" className={'delButton'} defaultValue="Delete geotag" onClick={(e) => {this.removeNode(node.getID());}}/>
										</form>
									</div>
								))}
							</FileDragAndDrop>
						</section>
					
					<div id="map" >
						<Map width={'100%'} height={'100%'} state={mapState} onAPIAvailable={function () { console.log('API loaded'); }} center={[55.754734, 37.583314]} zoom={10}>
							{this.state.appCtrl.getGeotagList().getList().map((node, i) => (
								node.isChecked() &&
									<Marker className={'marker'} key={'map_' + i} lat={node.getGeotag().getLatitude()} lon={node.getGeotag().getLongitude()} />
							))}
							{this.state.appCtrl.getCurrentPosition() != null &&
								<Marker lat={this.state.appCtrl.getCurrentPosition().getLatitude()} lon={this.state.appCtrl.getCurrentPosition().getLongitude()} />
							}
						</Map>
					</div>
				</div>
			</div>
		);
  }
}

export default ControlsComponent;